<?php

namespace CPTeam\Deploy\Assets\Webloader;

use Nette\Application\UI\Control;
use WebLoader\Nette\LoaderFactory;
use WebLoader\Nette\WebLoader;

abstract class AbstractControl extends Control
{
	
	const CSS_FILE = "styles.css";
	const JS_FILE = "assets.js";
	
	public function getAssetsDirName()
	{
		return $this->getPresenter()->context->getParameters()['assetsDirName'] . DIRECTORY_SEPARATOR;
	}
	
	public function getAssetsDir()
	{
		return $this->getPresenter()->context->getParameters()['assetsDir'] . DIRECTORY_SEPARATOR;
	}
	
	
	public function isDeployedMode()
	{
		return $this->getPresenter()->context->getParameters()['deployedMode'] === true;
	}
	
	public function getCompiler() {
		return $this->getControl()->getCompiler();
	}
	
	public function getGeneratedAsset() {
		return $this->getCompiler()->generate(false)[0];
	}
	
	/**
	 * @return WebLoader
	 */
	protected function getControl()
	{
		return $this["control"];
	}
	
	/**
	 * @return LoaderFactory
	 */
	protected function getLoaderFactory()
	{
		return $this->getPresenter()->context->getService("webloader.factory");
	}
	
	public function render()
	{
		if ($this->isDeployedMode()) {
			echo $this->getAssetHtml(), PHP_EOL;
			
			if (!empty($this->getPresenter()->context->getParameters()['webloader'][$this->getType()]["default"]["remoteFiles"])) {
				foreach ($this->getPresenter()->context->getParameters()['webloader'][$this->getType()]["default"]["remoteFiles"] as $file) {
					echo $this->getAssetHtml($file), PHP_EOL;
				}
			}
		} else {
			$this->getLoaderFactory();
			
			$lastModified = $this->getControl()->getCompiler()->getLastModified();
			
			$assetFile = $this->getAssetsDir() . $this->getAssetFile();
			
			if (file_exists($assetFile) == false || $lastModified > filemtime($assetFile)) {
				$this->compileAsset();
			}
			
			$this->getControl()->render();
		}
	}
	
	/**
	 * @return string
	 */
	public function getBasePath()
	{
		$path = "/" . $this->getAssetsDirName() . $this->getAssetFile();
		
		return $path . "?t=" . filemtime(WWW_DIR . $path);
	}
	
	abstract public function compileAsset();
	abstract public function createComponentControl();
	abstract public function getAssetFile();
	abstract public function getAssetHtml($source = null);
	abstract public function getType();
	
	
}
