<?php

namespace CPTeam\Deploy\Assets\Webloader;

use Nette\Utils\Html;

use WebLoader\Nette\JavaScriptLoader;

class CssControl extends AbstractControl
{
	
	public function getAssetHtml($source = null)
	{
		return Html::el("link")
			->rel("stylesheet")
			->type("text/css")
			->href($source ? $source : $this->getBasePath());
	}
	
	public function getAssetFile()
	{
		return self::CSS_FILE;
	}
	
	public function getType()
	{
		return "css";
	}
	
	public function compileAsset()
	{
		$generated = $this->getGeneratedAsset();
		
		$tempPath = WWW_DIR . $this->getControl()->getTempPath();
		$cssFile = $generated->file;
		
		$css = file_get_contents(
			$tempPath . DIRECTORY_SEPARATOR . $cssFile
		);
		
		$cssMin = \CssMin::minify($css, [
			'remove-last-semiciolon',
		]);
		
		file_put_contents($this->getAssetsDir() . $this->getAssetFile(), $cssMin);
	}
	
	
	/** @return JavaScriptLoader */
	public function createComponentControl()
	{
		return $this->getLoaderFactory()->createCssLoader('default');
	}

}

interface ICssFactory
{
	/** @return CssControl */
	public function create();
}
