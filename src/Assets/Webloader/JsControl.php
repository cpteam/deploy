<?php

namespace CPTeam\Deploy\Assets\Webloader;

use JShrink\Minifier;
use Nette\Utils\Html;

use WebLoader\Nette\JavaScriptLoader;

class JsControl extends AbstractControl
{
	
	public function getAssetHtml($source = null)
	{
		return Html::el("script")
			->src($source ? $source : $this->getBasePath());
	}
	
	public function getAssetFile()
	{
		return self::JS_FILE;
	}
	
	public function getType()
	{
		return "js";
	}
	
	public function compileAsset()
	{
		$generated = $this->getGeneratedAsset();
		
		$tempPath = WWW_DIR . $this->getControl()->getTempPath();
		
		$file = $generated->file;
		
		$js = file_get_contents($tempPath . DIRECTORY_SEPARATOR . $file);
		
		$jsMin = Minifier::minify($js);
		
		file_put_contents($this->getAssetsDir() . $this->getAssetFile(), $jsMin);
	}
	
	
	/** @return JavaScriptLoader */
	public function createComponentControl()
	{
		return $this->getLoaderFactory()->createJavaScriptLoader('default');
	}
	

}

interface IJsFactory
{
	/** @return JsControl */
	public function create();
}
