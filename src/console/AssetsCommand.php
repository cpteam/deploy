<?php
namespace CPTeam\Deploy\Console;

use Dmpm\Lib\Console\Core;
use Dmpm\Model\Service\TaxonService;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Webgarden\Deploy\Assets\Webloader\ICssFactory;
use Webgarden\Deploy\Assets\Webloader\IJsFactory;

/**
 * Class MemcacheTruncate
 * @package Dmpm\Lib\Console
 */
class AssetsCommand extends Core
{
	const OPT_CLEAR = "clear";
	const OPT_DELETE = "delete";
	
	/** @var TaxonService @inject */
	public $taxonService;
	
	/** @var ICssFactory @inject */
	public $cssControlFactory;
	
	/** @var IJsFactory @inject */
	public $jsControlFactory;
	
	protected function configure()
	{
		$this->setName('deploy:assets')
			->setDescription('Build assets');
		
		$this->addOption(self::OPT_CLEAR, "c", InputOption::VALUE_NONE, "Delete assets file before compile");
		$this->addOption(self::OPT_DELETE, "d", InputOption::VALUE_NONE, "Only delete assets");
	}
	
	/**
	 * @param InputInterface $input
	 * @param OutputInterface $output
	 */
	public function runCmd(InputInterface $input, OutputInterface $output)
	{
		$deleteBefore = $input->getOption(self::OPT_CLEAR) === true || $input->getOption(self::OPT_DELETE) === true;
		$compile = $input->getOption(self::OPT_DELETE) === false;
		
		
		$css = $this->cssControlFactory->create();
		$js = $this->jsControlFactory->create();
		
		$this->app->getPresenter()->addComponent($css, "css");
		$this->app->getPresenter()->addComponent($js, "ks");
		
		
		if ($deleteBefore) {
			unlink($css->getAssetsDir() . $css->getAssetFile());
			unlink($js->getAssetsDir() . $js->getAssetFile());
		}
		if ($compile) {
			$css->compileAsset();
			$js->compileAsset();
		}
		
	}

}
